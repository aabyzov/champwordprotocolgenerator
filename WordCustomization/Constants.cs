﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace WordCustomization
{
    class Constants
    {
        public static string DefaultJerseyLabel
        {
            get { return "б/н"; }
        }

        public static string EmptyJerseyLabel
        {
            get { return String.Empty; }
        }

        public static class Teams
        {
            public static string RTI
            {
                get { return "РТИ"; }
            }

            public static string Hyundai
            {
                get { return "Hyundai"; }
            }

            public static string STALogistic
            {
                get { return "STA Logistic"; }
            }

            public static string ShateM
            {
                get { return "ШАТЕ-М"; }
            }

            public static string DriveMotors
            {
                get { return "Drive Motors"; }
            }

            public static string Mazda
            {
                get { return "Mazda"; }
            }

            public static string ITBand
            {
                get { return "ITBand"; }
            }

            public static string Safari
            {
                get { return "Сафари"; }
            }

            public static string Ekospecservice
            {
                get { return "Экоспецсервис"; }
            }

            public static string Scorpion
            {
                get { return "Скорпион"; }
            }
        }

        public static int StartingParaghraph
        {
            get { return 21; }
        }

        public static int ParaghraphLineShift
        {
            get { return 8; }
        }

        public static int ParaghraphTeamShift
        {
            get { return ParaghraphLineShift / 2; } 
        }

        public static int ParaghraphPad
        {
            get { return 1; }
        }
    }
}
