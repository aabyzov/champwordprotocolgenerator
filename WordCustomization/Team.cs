﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace WordCustomization
{
    class Team
    {
        public string Name { get; set; }

        public IList<Player> Players { get; set; }

        public Player GetPlayer(int playerIndex)
        {
            if (playerIndex < Players.Count)
                return Players[playerIndex];
            return null;
        }
    }
	
	// line 1
	// line 2
}
