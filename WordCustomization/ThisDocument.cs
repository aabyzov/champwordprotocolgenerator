﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml.Linq;
using Microsoft.Office.Tools.Word;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Office = Microsoft.Office.Core;
using Word = Microsoft.Office.Interop.Word;

namespace WordCustomization
{
    public partial class ThisDocument
    {
        private void ThisDocument_Startup(object sender, System.EventArgs e)
        {
            var matches = Data.Schedule();

            foreach (var fixture in matches)
            {
                var teamA = fixture.Home;
                var teamB = fixture.Away;

                ReplaceTour(fixture.Tour);

                var initTeamsLine = ReplaceGameTitle(teamA.Name, teamB.Name);

                var maxPlayerCount = Math.Max(teamA.Players.Count, teamB.Players.Count);

                if (maxPlayerCount > 14) throw new InvalidOperationException("Too many players in a team");

                for (var i = 0; i < maxPlayerCount; i++)
                {
                    //home
                    var paragraph = Constants.StartingParaghraph + i * Constants.ParaghraphLineShift;
                    var player1 = teamA.GetPlayer(i);
                    if (player1 != null)
                    {
                        this.Paragraphs[paragraph].Range.Text = player1.JerseyLabel;
                        this.Paragraphs[paragraph + Constants.ParaghraphPad].Range.Text = player1.FullName;
                    }

                    //away
                    var awayParagraph = paragraph + Constants.ParaghraphTeamShift;
                    var player2 = teamB.GetPlayer(i);
                    if (player2 == null) continue;
                    this.Paragraphs[awayParagraph].Range.Text = player2.JerseyLabel;
                    this.Paragraphs[awayParagraph + Constants.ParaghraphPad].Range.Text = player2.FullName;
                }

                var tour = fixture.Tour;

                var gameName = tour + "." + teamA.Name + "-" + teamB.Name;

                object fileName = @"C:\Users\Anton\Google Drive\Minifootball.by\Бизнес-лига 2015-2016\Протоколы\Этап2" +
                                  gameName + ".doc";

                this.SaveAs(ref fileName,
                    ref missing, ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing, ref missing, ref missing);

                this.PrintOut(ref missing, ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);

                CleanUpFields(initTeamsLine);
            }
        }

        private void ReplaceTour(int tour)
        {
            this.Paragraphs[1].Range.Text = "ТУР №" + tour + "\r";
        }

        private void CleanUpFields(string initTeamsLine)
        {
            for (var i = 0; i < 14; i++)
            {
                //home
                var paragraph = Constants.StartingParaghraph + i * Constants.ParaghraphLineShift;
                
                    this.Paragraphs[paragraph].Range.Text = String.Empty;
                    this.Paragraphs[paragraph + Constants.ParaghraphPad].Range.Text = String.Empty;
                

                //away
                var awayParagraph = paragraph + Constants.ParaghraphTeamShift;
                this.Paragraphs[awayParagraph].Range.Text = String.Empty;
                this.Paragraphs[awayParagraph + Constants.ParaghraphPad].Range.Text = String.Empty;
            }

            this.Paragraphs[4].Range.Text = initTeamsLine;
            this.Paragraphs[1].Range.Text = "ТУР №\r";
        }

        private string ReplaceGameTitle(string teamA, string teamB)
        {
            var teamsLine = this.Paragraphs[4].Range.Text;
            var match = Regex.Match(teamsLine, "_+");
            if (!match.Success) throw new InvalidOperationException("Template is incorrect");

            var placeholder1 = match.Value;

            var nextMatch = match.NextMatch();
            if (!nextMatch.Success) throw new InvalidOperationException("Template is incorrect");
            var placeholder2 = nextMatch.Value;

            var centeredTeam1 = teamA.CenterString(placeholder1.Length);
            var centeredTeam2 = teamB.CenterString(placeholder2.Length);

            var intermediateResult = teamsLine.Replace(placeholder1, centeredTeam1);
            this.Paragraphs[4].Range.Text = intermediateResult.Replace(placeholder2, centeredTeam2);
            return teamsLine;
        }


        private void ThisDocument_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisDocument_Startup);
            this.Shutdown += new System.EventHandler(ThisDocument_Shutdown);
        }

        #endregion
    }


    

   

}


