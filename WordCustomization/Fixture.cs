﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordCustomization
{
    class Fixture
    {

        public Team Home { get; set; }
        public Team Away { get; set; }

        public int Tour { get; set; }
    }
}
