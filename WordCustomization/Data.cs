﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordCustomization
{
    class Data
    {
        static Data()
        {
            Teams = new List<Team>()
            {
                new Team()
                {
                    Name = Constants.Teams.RTI,
                    Players = new List<Player>()
                    {
                       CreatePlayer("Тимашов", "Александр", "3"),
                       CreatePlayer("Мицкевич", "Сергей", "9"),
                       CreatePlayer("Кудинович", "Сергей", Constants.DefaultJerseyLabel),
                       CreatePlayer("Силин", "Сергей", "16"),
                       CreatePlayer("Стотик", "Виталий", "10"),
                       CreatePlayer("Черный", "Владимир", "1"),
                       CreatePlayer("Жванский", "Николай", "29"),
                       CreatePlayer("Мацкевич", "Алексей", "8"),                        
                       CreatePlayer("Кузнецов", "Роман", "8"),                        
                    }
                },
                new Team()
                {
                    Name = Constants.Teams.Hyundai,
                    Players = new List<Player>()
                    {
                        CreatePlayer("Шинтарь", "Максим", "1"),
                        CreatePlayer("Савко", "Евгений", "9"),
                        CreatePlayer("Тарасов", "Дмитрий", "3"),
                        CreatePlayer("Шимло", "Максим", "4"),
                        CreatePlayer("Мельников", "Артур", "8"),
                        CreatePlayer("Тарасевич", "Илья", "5"),
                        CreatePlayer("Сакович", "Вячеслав", "17"),
                        CreatePlayer("Бондарчук", "Игнат", "16"),
                        CreatePlayer("Антонов", "Никита", "2"),
                        CreatePlayer("Фурс", "Александр", Constants.DefaultJerseyLabel),
                        CreatePlayer("Подольский", "Антон"),
                        CreatePlayer("Вануян", "Андрей"),
                        CreatePlayer("Бенаревич", "Егор"),
                        CreatePlayer("Бибик", "Дмитрий", Constants.DefaultJerseyLabel),
                    }
                },

                new Team()
                {
                    Name = Constants.Teams.ShateM,
                    Players = new List<Player>()
                    {
                        CreatePlayer("Лисянович", "Александр", "18"),
                        CreatePlayer("Орешников", "Дмитрий", "1"),
                        CreatePlayer("Стрига", "Виталий", "14"),
                        CreatePlayer("Дроздович", "Дмитрий", "10"),
                        CreatePlayer("Куницкий", "Кирилл", "22"),
                        CreatePlayer("Левкович", "Алексей", "20"),
                        CreatePlayer("Наумов", "Алексей", "19"),
                        CreatePlayer("Лисица", "Михаил", "21"),
                        CreatePlayer("Беленин", "Олег", "2"),
                        CreatePlayer("Лущинский", "Михаил", "2"),
                    }
                },

                new Team()
                {
                    Name = Constants.Teams.Mazda,
                    Players = new List<Player>()
                    {
                        CreatePlayer("Гаврюшин", "Игорь", "10"),
                        CreatePlayer("Лынько", "Василий", "7"),
                        CreatePlayer("Юралевич", "Роман", "25"),
                        CreatePlayer("Зеневич", "Андрей", Constants.DefaultJerseyLabel),
                        CreatePlayer("Жуковский", "Сергей", "17"),
                        CreatePlayer("Кравченя", "Николай", "9"),
                        CreatePlayer("Костючков", "Артем", "28"),
                        CreatePlayer("Козырев", "Виктор", "1"),
                        CreatePlayer("Федосеенко", "Николай", "84")
                    }
                },

                new Team()
                {
                    Name = Constants.Teams.STALogistic,
                    Players = new List<Player>()
                    {
                        CreatePlayer("Аринович", "Сергей", "58"),
                        CreatePlayer("Богомолов", "Антон", "3"),
                        CreatePlayer("Лопатин", "Игорь", "7"),
                        CreatePlayer("Заричный", "Олег", "13"),
                        CreatePlayer("Гурленя", "Денис", "10"),
                        CreatePlayer("Ковалев", "Игорь", "31"),
                        CreatePlayer("Рудь", "Франц", "20"),
                        CreatePlayer("Матыяшкойть", "Илья", "11"),
                        CreatePlayer("Савко", "Александр", "15"),
                        CreatePlayer("Попов", "Александр", "17"),
                        CreatePlayer("Соколик", "Олег", "33"),
                        CreatePlayer("Пупин", "Виталий", "8"),
                        CreatePlayer("Драко", "Игорь", "1"),
                        CreatePlayer("Милькевич", "Павел", "5"),
                    }
                },

                new Team()
                {
                    Name = Constants.Teams.DriveMotors,
                    Players = new List<Player>()
                    {
                        CreatePlayer("Верин", "Андрей", Constants.EmptyJerseyLabel),
                        CreatePlayer("Щурок", "Виталий", Constants.EmptyJerseyLabel),
                        CreatePlayer("Герасимович", "Александр", Constants.EmptyJerseyLabel),
                        CreatePlayer("Анисько", "Евгений", Constants.EmptyJerseyLabel),
                        CreatePlayer("Шпак", "Кирилл", Constants.EmptyJerseyLabel),
                        CreatePlayer("Шанкин", "Дмитрий", Constants.EmptyJerseyLabel),
                        CreatePlayer("Левкевич", "Михаил", Constants.EmptyJerseyLabel),
                        CreatePlayer("Сергей", "Руслан", Constants.EmptyJerseyLabel),
                        CreatePlayer("Гапанович", "Вячеслав", Constants.EmptyJerseyLabel),
                        CreatePlayer("Машко", "Владимир", Constants.EmptyJerseyLabel),
                        
                    }
                },

                new Team()
                {
                    Name = Constants.Teams.Safari,
                    Players = new List<Player>()
                    {
                        CreatePlayer("Алешкович", "Владимир", "1"),
                        CreatePlayer("Черепковский", "Денис", Constants.EmptyJerseyLabel),
                        CreatePlayer("Бабинцев", "Олег", Constants.EmptyJerseyLabel),
                        CreatePlayer("Маслов", "Денис", Constants.EmptyJerseyLabel),
                        CreatePlayer("Гурленя", "Николай", Constants.EmptyJerseyLabel),
                        CreatePlayer("Тавгень", "Вадим", Constants.EmptyJerseyLabel),
                        CreatePlayer("Шпилевский", "Алексей", Constants.EmptyJerseyLabel),
                        CreatePlayer("Лазинский", "Андрей", Constants.EmptyJerseyLabel),
                        CreatePlayer("Соколовский", "Дмитрий", Constants.EmptyJerseyLabel),
                    }
                },

                new Team()
                {
                    Name = Constants.Teams.ITBand,
                    Players = new List<Player>()
                    {
                        CreatePlayer("Фалинский", "Андрей", Constants.EmptyJerseyLabel),
                        CreatePlayer("Пумпур", "Вячеслав", Constants.EmptyJerseyLabel),
                        CreatePlayer("Куцый", "Александр", Constants.EmptyJerseyLabel),
                        CreatePlayer("Лазутин", "Виктор", Constants.EmptyJerseyLabel),
                        CreatePlayer("Крезо", "Валентин", Constants.EmptyJerseyLabel),
                        CreatePlayer("Круппа", "Дмитрий", Constants.EmptyJerseyLabel),
                        CreatePlayer("Капельчик", "Андрей", Constants.EmptyJerseyLabel),
                        CreatePlayer("Шпаковский", "Вадим", Constants.EmptyJerseyLabel),
                        CreatePlayer("Кунцевич", "Антон", Constants.EmptyJerseyLabel),
                        CreatePlayer("Чурадзе", "Дмитрий", Constants.EmptyJerseyLabel),
                        CreatePlayer("Радкевич", "Александр", Constants.EmptyJerseyLabel),
                        CreatePlayer("Витенко", "Алексей", Constants.EmptyJerseyLabel),
                        CreatePlayer("Андросенко", "Артем", Constants.EmptyJerseyLabel),
                        CreatePlayer("Зенькевич", "А.", Constants.EmptyJerseyLabel),
                    }
                },

                new Team()
                {
                    Name = Constants.Teams.Scorpion,
                    Players = new List<Player>()
                    {
                        CreatePlayer("Васильев", "Илья", "2"),
                        CreatePlayer("Синица", "Владимир", "16"),
                        CreatePlayer("Дащинский", "Сергей", "55"),
                        CreatePlayer("Степанов", "Александр", "9"),
                        CreatePlayer("Почесуй", "Игорь", "86"),
                        CreatePlayer("Бернович", "Дмитрий", "11"),
                        CreatePlayer("Кунаш", "Сергей", "32"),
                        CreatePlayer("Ярославцев", "Дмитрий", "14"),
                        CreatePlayer("Бабареко", "Андрей", "1"),
                        CreatePlayer("Баханович", "Дмитрий", Constants.EmptyJerseyLabel),
                    }
                },

                new Team()
                {
                    Name = Constants.Teams.Ekospecservice,
                    Players = new List<Player>()
                    {
                        CreatePlayer("Дядюля", "Николай", "2"),
                        CreatePlayer("Яшмаков", "Вадим", "16"),
                        CreatePlayer("Бабко", "Алексей", "55"),
                        CreatePlayer("Тур", "Дмитрий", "9"),
                        CreatePlayer("Козлов", "Дмитрий", "86"),
                        CreatePlayer("Сашко", "Вадим", "11"),
                        CreatePlayer("Таран", "Андрей", "32"),
                        CreatePlayer("Козлов", "Денис", "14"),
                        CreatePlayer("Суша", "Юрий", "3")
                    }
                },
            };

            Table = new List<Fixture>()
            {
                // CreateFixture(GetTeam(Constants.Teams.RTI), GetTeam(Constants.Teams.Mazda), 1),
                //CreateFixture(GetTeam(Constants.Teams.ShateM), GetTeam(Constants.Teams.Hyundai), 1),
                //CreateFixture(GetTeam(Constants.Teams.STALogistic), GetTeam(Constants.Teams.DriveMotors), 1),
                //CreateFixture(GetTeam(Constants.Teams.Scorpion), GetTeam(Constants.Teams.Ekospecservice), 1),
                //CreateFixture(GetTeam(Constants.Teams.Safari), GetTeam(Constants.Teams.ITBand), 1),

                //CreateFixture(GetTeam(Constants.Teams.RTI), GetTeam(Constants.Teams.Hyundai), 2),
                //CreateFixture(GetTeam(Constants.Teams.Mazda), GetTeam(Constants.Teams.Safari), 2),
                //CreateFixture(GetTeam(Constants.Teams.ShateM), GetTeam(Constants.Teams.DriveMotors), 2),
                //CreateFixture(GetTeam(Constants.Teams.STALogistic), GetTeam(Constants.Teams.Ekospecservice), 2),
                //CreateFixture(GetTeam(Constants.Teams.ITBand), GetTeam(Constants.Teams.Scorpion), 2),

                //CreateFixture(GetTeam(Constants.Teams.ITBand), GetTeam(Constants.Teams.STALogistic), 3),
                //CreateFixture(GetTeam(Constants.Teams.DriveMotors), GetTeam(Constants.Teams.RTI), 3),
                //CreateFixture(GetTeam(Constants.Teams.Hyundai), GetTeam(Constants.Teams.Mazda), 3),
                //CreateFixture(GetTeam(Constants.Teams.ShateM), GetTeam(Constants.Teams.Ekospecservice), 3),
                //CreateFixture(GetTeam(Constants.Teams.Safari), GetTeam(Constants.Teams.Scorpion), 3),

                //CreateFixture(GetTeam(Constants.Teams.RTI), GetTeam(Constants.Teams.Ekospecservice), 4),
                //CreateFixture(GetTeam(Constants.Teams.ShateM), GetTeam(Constants.Teams.ITBand), 4),
                // CreateFixture(GetTeam(Constants.Teams.STALogistic), GetTeam(Constants.Teams.Safari), 13),
                // CreateFixture(GetTeam(Constants.Teams.Scorpion), GetTeam(Constants.Teams.Mazda), 13),
                // CreateFixture(GetTeam(Constants.Teams.DriveMotors), GetTeam(Constants.Teams.Hyundai), 13),

                //   CreateFixture(GetTeam(Constants.Teams.RTI), GetTeam(Constants.Teams.STALogistic), 14),
                // CreateFixture(GetTeam(Constants.Teams.ShateM), GetTeam(Constants.Teams.Mazda), 14),
                // CreateFixture(GetTeam(Constants.Teams.DriveMotors), GetTeam(Constants.Teams.Safari), 14),
                // CreateFixture(GetTeam(Constants.Teams.Scorpion), GetTeam(Constants.Teams.Hyundai), 14),
                // CreateFixture(GetTeam(Constants.Teams.ITBand), GetTeam(Constants.Teams.Ekospecservice), 14),

                //CreateFixture(GetTeam(Constants.Teams.RTI), GetTeam(Constants.Teams.ShateM), 15),
                // CreateFixture(GetTeam(Constants.Teams.STALogistic), GetTeam(Constants.Teams.Hyundai), 15),
                // CreateFixture(GetTeam(Constants.Teams.DriveMotors), GetTeam(Constants.Teams.Scorpion), 15),
                // CreateFixture(GetTeam(Constants.Teams.Ekospecservice), GetTeam(Constants.Teams.Safari), 15),
                // CreateFixture(GetTeam(Constants.Teams.ITBand), GetTeam(Constants.Teams.Mazda), 15),

                // CreateFixture(GetTeam(Constants.Teams.RTI), GetTeam(Constants.Teams.Scorpion), 16),
                // CreateFixture(GetTeam(Constants.Teams.ShateM), GetTeam(Constants.Teams.STALogistic), 16),
                // CreateFixture(GetTeam(Constants.Teams.Ekospecservice), GetTeam(Constants.Teams.Mazda), 16),
                // CreateFixture(GetTeam(Constants.Teams.Hyundai), GetTeam(Constants.Teams.Safari), 16),
                // CreateFixture(GetTeam(Constants.Teams.DriveMotors), GetTeam(Constants.Teams.ITBand), 16),

                //  CreateFixture(GetTeam(Constants.Teams.RTI), GetTeam(Constants.Teams.Safari), 17),
                // CreateFixture(GetTeam(Constants.Teams.ShateM), GetTeam(Constants.Teams.Scorpion), 17),
                // CreateFixture(GetTeam(Constants.Teams.STALogistic), GetTeam(Constants.Teams.Mazda), 17),
                // CreateFixture(GetTeam(Constants.Teams.DriveMotors), GetTeam(Constants.Teams.Ekospecservice), 17),
                // CreateFixture(GetTeam(Constants.Teams.ITBand), GetTeam(Constants.Teams.Hyundai), 17),

                //   CreateFixture(GetTeam(Constants.Teams.RTI), GetTeam(Constants.Teams.ITBand), 18),
                // CreateFixture(GetTeam(Constants.Teams.ShateM), GetTeam(Constants.Teams.Safari), 18),
                // CreateFixture(GetTeam(Constants.Teams.STALogistic), GetTeam(Constants.Teams.Scorpion), 18),
                // CreateFixture(GetTeam(Constants.Teams.DriveMotors), GetTeam(Constants.Teams.Mazda), 18),
                // CreateFixture(GetTeam(Constants.Teams.Ekospecservice), GetTeam(Constants.Teams.Hyundai), 18),

                 //CreateFixture(GetTeam(Constants.Teams.ITBand), GetTeam(Constants.Teams.Safari), 20),
                 //CreateFixture(GetTeam(Constants.Teams.Hyundai), GetTeam(Constants.Teams.Mazda), 20),
                 //CreateFixture(GetTeam(Constants.Teams.STALogistic), GetTeam(Constants.Teams.DriveMotors), 20),
                 //CreateFixture(GetTeam(Constants.Teams.RTI), GetTeam(Constants.Teams.Scorpion), 20),

                //CreateFixture(GetTeam(Constants.Teams.Hyundai), GetTeam(Constants.Teams.RTI), 21),
                //CreateFixture(GetTeam(Constants.Teams.STALogistic), GetTeam(Constants.Teams.ShateM), 21),
                //CreateFixture(GetTeam(Constants.Teams.Safari), GetTeam(Constants.Teams.DriveMotors), 21),
                //CreateFixture(GetTeam(Constants.Teams.Ekospecservice), GetTeam(Constants.Teams.Scorpion), 21),

                //CreateFixture(GetTeam(Constants.Teams.ITBand), GetTeam(Constants.Teams.ShateM), 22),
                //CreateFixture(GetTeam(Constants.Teams.Hyundai), GetTeam(Constants.Teams.Scorpion), 22),
                //CreateFixture(GetTeam(Constants.Teams.Ekospecservice), GetTeam(Constants.Teams.Mazda), 22),
                //CreateFixture(GetTeam(Constants.Teams.STALogistic), GetTeam(Constants.Teams.Safari), 22),

                //CreateFixture(GetTeam(Constants.Teams.ITBand), GetTeam(Constants.Teams.DriveMotors), 23),
                //CreateFixture(GetTeam(Constants.Teams.ShateM), GetTeam(Constants.Teams.Safari), 23),
                //CreateFixture(GetTeam(Constants.Teams.Mazda), GetTeam(Constants.Teams.RTI), 23),
                //CreateFixture(GetTeam(Constants.Teams.Hyundai), GetTeam(Constants.Teams.Ekospecservice), 23),

            };
        }
        public static IList<Team> Teams { get; set; }
        public static IList<Fixture> Table { get; set; }

        public static IList<Fixture> Schedule(int tour = 0)
        {
            if (tour != 0)
                return Table.Where(x => x.Tour == tour).ToList();
            return Table;
        }

        private static Player CreatePlayer(string lname, string fname, string jerseyLabel = "")
        {
            return new Player() { FirstName = fname, LastName = lname, JerseyLabel = jerseyLabel };
        }

        private static Fixture CreateFixture(Team home, Team away, int tour)
        {
            return new Fixture() { Home = home, Away = away, Tour = tour };
        }

        private static Team GetTeam(string name)
        {
            return Teams.First(x => x.Name == name);
        }


    }
}
